import React, { Component } from 'react';
import { View, Text, Image, Dimensions, Share, TouchableOpacity } from 'react-native';
import StarRating from 'react-native-star-rating';

import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import { Header, Left, Body, Content, Right } from 'native-base';
import Loading from '../Loading';
const { width, height } = require('Dimensions').get('window');
class Book extends Component {

    constructor(props) {
        super(props);
        this.state = {
            book_title: '',
            book_img: require('./../../assets/loader.gif'),
            coverImgWidth: 320,
            coverImgHeight: 200,
            bookImgWidth: 320,
            bookImgHeight: 200,
            is_loading: true,
            share_url: 'http://alorpathshala.org/',
            book_authors: [],
            book_description: '',
            publication_id: '',
            publication_name: '',
            is_loading: true,
            book_rating: 0
        }
    }
    back() {
        Actions.pop()
    }

    shareBook() {
        Share.share({
            message: this.props.title,
            url: this.state.share_url,
            title: this.props.title
        }, {
                // Android only:
                dialogTitle: 'Share ' + this.props.title,
                // iOS only:
                excludedActivityTypes: [
                    'com.apple.UIKit.activity.PostToTwitter'
                ]
            })
    }

    componentWillMount() {

        this.getImgSize();

        fetch("http://192.168.0.108/pathshala/public/api/book/details?book_id=" + this.props.book_id)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    share_url: responseJson.share_url,
                    book_authors: responseJson.authors,
                    book_description: responseJson.book_description,
                    publication_id: responseJson.publication_id,
                    publication_name: responseJson.publication_name,
                    book_rating: responseJson.rating
                });
            }).then(() => {
                this.setState({ is_loading: false });
            })
            .catch((error) => {
                console.error(error);
            });
    }
    getImgSize() {
        Image.getSize(this.props.bookUrl.uri, (width, height) => {
            //  calculate image width and height 
            const screenWidth = Dimensions.get('window').width;
            const scaleFactor = width / screenWidth;
            const imageHeight = height / scaleFactor;
            this.setState({ bookImgWidth: screenWidth, bookImgHeight: imageHeight })
        })
        Image.getSize('http://alorpathshala.org/web/img/top-banner.jpg', (width, height) => {
            //  calculate image width and height 
            const screenWidth = Dimensions.get('window').width;
            const scaleFactor = width / screenWidth;
            const imageHeight = height / scaleFactor;
            this.setState({ coverImgWidth: screenWidth, coverImgHeight: imageHeight })
        })

    }
    onStarRatingPress(rating) {
        alert(rating);
    }
    render() {

        return (
            <Content>
                <Header style={{ backgroundColor: 'rgb(66, 103, 178)' }}>
                    <Left>
                        <Icon style={{ paddingLeft: 5 }} onPress={() => this.back()} name={"ios-arrow-round-back"} size={30} color={"#fff"} />
                    </Left>
                    <View style={styles.searchSection}>

                        <Text style={styles.headerTitle} >
                            {this.props.title}
                        </Text>
                    </View>
                    <Right>
                        <Icon style={{ padding: 10 }} onPress={() => this.shareBook()} name={"md-share"} size={30} color={"#fff"} />
                    </Right>
                </Header>
                {this.state.is_loading == true ? <Loading /> :
                    <Body>
                        <Image
                            resizeMode="contain"
                            style={{ width: this.state.coverImgWidth, height: this.state.coverImgHeight, marginBottom: 10 }}
                            source={require('./../../assets/top-banner.jpg')} />


                        <Image style={{
                            width: this.state.bookImgWidth,
                            height: this.state.bookImgHeight,
                            borderWidth: 0,
                        }} source={this.props.bookUrl} />

                        <Text style={{ fontSize: 22, paddingTop: 20, fontWeight: '700' }}>
                            {this.props.title}
                        </Text>
                        <View>
                            <StarRating
                                disabled={false}
                                halfStarEnabled={false}
                                fullStarColor='gold'
                                maxStars={10}
                                selectedStar={(rating) => this.onStarRatingPress(rating)}
                                starSize={20}
                                rating={this.state.book_rating}
                            />
                        </View>
                        <View style={{ flex: 1, paddingTop: 20, flexDirection: 'row', width: width }}>
                            <View style={{ flex: 1, borderRightColor: 'gray', borderRightWidth: 3 }}>
                                {this.state.book_authors != false &&
                                    <View style={{ alignItems: 'center', alignContent: 'center', justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 20 }}>লেখক</Text>
                                        {this.state.book_authors.map((author, index) => {
                                            return (<Text style={{ fontSize: 18 }} key={index}>{author.author_name}</Text>)
                                        })}
                                    </View>
                                }
                            </View>
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <Text style={{ fontSize: 20 }}>প্রকাশনী</Text>
                                <Text style={{ fontSize: 18 }}>{this.state.publication_name}</Text>
                            </View>
                        </View>
                    </Body>
                }
            </Content>
        )
    }
}
export default Book;
const styles = {
    searchSection: {
        flex: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(66, 103, 178, 0.95)',
        color: '#fff',
        marginBottom: 10,
        paddingTop: 10,
    },
    headerTitle: {
        fontSize: 15,
        color: '#fff',
    },
    coverImg: {
        width,
        height

    }

}