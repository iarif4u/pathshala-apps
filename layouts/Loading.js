import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, StyleSheet } from 'react-native';

class Loading extends Component {
   render() {
      return (
         <View style = {styles.container}>
            <ActivityIndicator
               animating = {true}
               color = '#4267b2'
               size = "large"
               style = {styles.activityIndicator}/>
         </View>
      )
   }
}
export default Loading;

const styles = StyleSheet.create ({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 50
   },
   activityIndicator: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 20
   }
})