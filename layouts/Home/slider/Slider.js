
import React,{Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity,Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
//require('../../../assets/slider/slider-01.jpg')
export default class Slider extends Component {
  getDetailInfo(content_id,content,title,imgUrl){
      if(content==='book'){
        Actions.book({'book_id':content_id,'title':title,bookUrl:imgUrl});
      }else{
        alert(content_id+" Author: "+content);
      }
      
    }
    render() {
      
      return (
        
        <TouchableOpacity style={{height:200,marginBottom:15}} onPress={()=>this.getDetailInfo(this.props.content_id,this.props.content,this.props.slide_caption,this.props.imgUri)}>
        <View style={Style.slider}>
        <Image style={Style.slide} source={this.props.imgUri} />
       
          <Text style={Style.caption}>
            {this.props.slide_caption}
          </Text>
      
        </View>
        </TouchableOpacity>
      );
    }
  }
  const Style =  StyleSheet.create({
    bigblue: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 30,
    },
    slider:{
      width:200,
      height:200,
      marginLeft: 20,
      borderWidth: 0,
    },
  
    view:{
      flex:1,
      width:300,
      height:200
    },
    caption:{
      fontSize:18,
      paddingLeft: 10,
      paddingTop: 5,
      color:'gray',
      alignItems: 'center',
      flex: 1,
    },
    slide:{
      marginTop: 10,
      flex: 3,
      resizeMode:'cover'
    }
  });