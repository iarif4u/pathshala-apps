import React,{Component} from 'react';
import {StatusBar, StyleSheet, Text, View, ScrollView, Image, Dimensions,SafeAreaView } from 'react-native';
import Slider from './slider/Slider';
import Loading from './../Loading'

export default class Home extends Component {
  constructor(props){
    super(props);
    this.state = {
      brief_title: '',
      brief_info : '',
      slides:[],
      coverImg:require('../../assets/loader.gif'),
      coverImgWidth: 100,
      coverImgHeight: 200,
      isLoading: true
    }
  }
  componentDidUpdate(){
    if(this.state.isLoading){
      setTimeout(()=>{
        this.setState({
          isLoading: false,
        });
      },1000);
    }
  }
  componentWillMount(){
    fetch('http://alorpathshala.org/api/brief/info')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          brief_title: responseJson.brief,
          brief_info:responseJson.content,
          slides:responseJson.slides
        });

      })
      .catch((error) =>{
        console.error(error);
      });

      Image.getSize('http://alorpathshala.org/web/img/top-banner.jpg', (width, height) => {
       //  calculate image width and height 
        const screenWidth = Dimensions.get('window').width;      
        const scaleFactor = width / screenWidth;
        const imageHeight = height / scaleFactor;
        this.setState({coverImgWidth: screenWidth, coverImgHeight: imageHeight,coverImg:{uri:'http://alorpathshala.org/web/img/top-banner.jpg'}})
      })
  }
  render() {
    if(!this.state.isLoading){
  return (
    <SafeAreaView>
    <StatusBar hidden = {true} backgroundColor="blue" barStyle="light-content"/>
    <ScrollView>
    <View>
      <View style={{justifyContent:'center',alignItems:'center'}}>
      <Image resizeMode="contain" style={{width: this.state.coverImgWidth, height: this.state.coverImgHeight}} source={this.state.coverImg} />
      </View>
     
      { this.state.slides.length>0 && 
      <ScrollView scrollEventThrottle={5}>
            <View style={{  height:200}}>
                <ScrollView horizontal={true}>
                    {this.state.slides.map(slide=>{
                      return(
                        <Slider key={slide.image} slide_caption={slide.caption} imgUri={{uri:slide.path}}/>
                      )
                    })}
                  
                </ScrollView>
                
            </View>
        </ScrollView> 
      } 
      { this.state.brief_title.length >0 && 
        <View style={Style.container}>
          <Text style={Style.tagTitle}>
            {this.state.brief_title}
          </Text>
          <Text style={Style.tagDetails}>
            {this.state.brief_info}
          </Text>
        </View>
      }
        </View>
    </ScrollView>
    </SafeAreaView>
  );
    }else{
     return <Loading/>
    }
  }
}
  const Style =  StyleSheet.create({
    alokitoTitle:{
        fontSize:80,
        color:'red',
        
    },
   
    folatBtn:{
      backgroundColor: '#fc454e',
      width: 66,
      height: 66,
      borderRadius: 33,
      justifyContent: 'center',
      alignItems:'center',
      position: 'absolute',
      top: 200,
      left: -5
    },
    canvas: {
      top: 0,
      left: 0,
      right: 0,
    },
    tagDetails:{
      fontSize:18,
      color:'#8C5902',
      textAlign: 'center',
    },
    container: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      borderWidth: 1,
      borderColor: '#e3e3e3',
      padding: 10,
      margin: 10,
    },
    bigblue: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 30,
    },
    slider:{
      width:300,
      height:200,
      marginLeft: 20,
    },
    red: {
      color: 'red',
    },
    view:{
      width:300,
      height:200
    },
    tagTitle:{
      fontSize:30,
      alignItems:'center',
      paddingTop: 10,
      color:'#A35757',
      alignItems: 'center',
    },
    slide:{
      marginTop: 10,
      resizeMode:'cover'
    }
  });