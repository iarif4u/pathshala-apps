
import React,{Component} from 'react';
import {StatusBar, StyleSheet, Text, View, SafeAreaView, Image,Dimensions} from 'react-native';
import Slideshow from 'react-native-image-slider-show';
import Loading from './../Loading';
import Slider from './slider/Slider';
import { ScrollView } from 'react-native-gesture-handler';
export default class Elibaray extends Component {

    constructor(props){
        super(props);
        this.state = {
          brief_title: '',
          brief_info : '',
          slides:[],
          authors:[],
          coverImg:false,
          coverImgWidth: 100,
          coverImgHeight: 200,
          isLoading: true
        }
      }
      componentDidUpdate(){
        if(this.state.isLoading){
          setTimeout(()=>{
            this.setState({
              isLoading: false,
            });
          },1000);
        }
      }
      componentWillMount(){
        fetch('http://alorpathshala.org/api/brief/info')
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              brief_title: responseJson.brief,
              brief_info:responseJson.content
            });
    
          })
          .catch((error) =>{
            console.error(error);
          });
          
          fetch('http://192.168.0.108/pathshala/public/api/recent')
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              slides:responseJson.books,
              authors:responseJson.authors,
            });
    
          })
          .catch((error) =>{
            console.error(error);
          });
    
          Image.getSize('http://alorpathshala.org/web/img/top-banner.jpg', (width, height) => {
           //  calculate image width and height 
            const screenWidth = Dimensions.get('window').width;      
            const scaleFactor = width / screenWidth;
            const imageHeight = height / scaleFactor;
            this.setState({coverImgWidth: screenWidth, coverImgHeight: imageHeight})
          })
      }


    render() {
      return (
        <SafeAreaView>
        <StatusBar hidden = {true} backgroundColor="blue" barStyle="light-content"/>
        <ScrollView>
        <View style={{justifyContent:'center',alignItems:'center', marginBottom:5,}}>
        {this.state.isLoading==false ?
          <Image 
          resizeMode="contain" 
          style={{width: this.state.coverImgWidth, height: this.state.coverImgHeight}} 
          source={require('./../../assets/top-banner.jpg')} />
          :
          <Loading/>
        }
        </View>
        { this.state.isLoading==false && 
        <Slideshow style={{marginTop:20}}
        dataSource={[
            { url:'http://alorpathshala.org/web/slide/1542540369.jpg' },
            { url:'http://alorpathshala.org/web/slide/1542540439.jpg' },
            { url:'http://alorpathshala.org/web/slide/1542540454.jpg' },
            { url:'http://alorpathshala.org/web/slide/1542540474.jpg' },
        ]}/>
      }
        { this.state.isLoading==false && 
            <View style={Style.container}>
              <Text style={Style.tagTitle}>
                {this.state.brief_title}
              </Text>
              <Text style={Style.tagDetails}>
                {this.state.brief_info}
              </Text>
            </View>
          }

          { this.state.isLoading==false && 
            <ScrollView scrollEventThrottle={5}>
                <Text style={Style.slideTitle}>সাম্প্রতিক বইসমূহ</Text>
                  <View>
                      <ScrollView horizontal={true}>
                          {this.state.slides.map(slide=>{
                            return(
                              <Slider content="book" content_id={slide.id} key={slide.id} slide_caption={slide.title} imgUri={{uri:slide.feature_image}}/>
                            )
                          })}
                        
                      </ScrollView>
                      
                  </View>
                  <Text style={Style.slideTitle}>জনপ্রিয় লেখকবৃন্দ</Text>
                  <View>
                      <ScrollView horizontal={true}>
                          {this.state.authors.map(author=>{
                            return(
                              <Slider content="author" content_id={author.id} key={author.id} slide_caption={author.name} imgUri={{uri:author.profile_img}}/>
                            )
                          })}
                        
                      </ScrollView>
                      
                  </View>
              </ScrollView> 

              
            } 
          </ScrollView>

        </SafeAreaView>
      );
    }
  }


  const Style =  StyleSheet.create({
    alokitoTitle:{
        fontSize:80,
        color:'red',
        
    },
    slideTitle:{
        fontSize:22,
        marginLeft:20,
    },
   
    folatBtn:{
      backgroundColor: '#fc454e',
      width: 66,
      height: 66,
      borderRadius: 33,
      justifyContent: 'center',
      alignItems:'center',
      position: 'absolute',
      top: 200,
      left: -5
    },
    canvas: {
      top: 0,
      left: 0,
      right: 0,
    },
    tagDetails:{
      fontSize:18,
      color:'#8C5902',
      textAlign: 'center',
    },
    container: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      borderWidth: 1,
      borderColor: '#e3e3e3',
      padding: 10,
      margin: 10,
    },
    bigblue: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 30,
    },
    slider:{
      width:300,
      height:200,
      marginLeft: 20,
    },
    red: {
      color: 'red',
    },
    view:{
      width:300,
      height:200
    },
    tagTitle:{
      fontSize:30,
      alignItems:'center',
      paddingTop: 10,
      color:'#A35757',
      alignItems: 'center',
    },
    slide:{
      marginTop: 10,
      resizeMode:'cover'
    }
  });