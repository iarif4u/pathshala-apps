
import React, { Component } from 'react';
import { StyleSheet, Text, View, SafeAreaView, WebView } from 'react-native';
import Loading from './../Loading'
export default class Rules extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      rules_title: '',
      ruels_text: '',
      isLoading: true
    }
  }
  componentDidUpdate() {
    if (this.state.isLoading) {
      setTimeout(() => {
        this.setState({
          isLoading: false,
        });
      }, 1000);
    }
  }

  componentWillMount() {
    fetch('http://alorpathshala.org/api/rules')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          rules_title: responseJson.title,
          ruels_text: responseJson.content
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    if (!this.state.isLoading) {
      return (

        <WebView
          style={{ padding: 15 }}
          originWhitelist={['*']}
          source={{ html: this.state.ruels_text }}
        />

      );
    } else {
      return <Loading />
    }
  }
}