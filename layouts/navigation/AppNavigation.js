import { createMaterialTopTabNavigator, DrawerNavigator,createDrawerNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import { Ionicons } from '@expo/vector-icons';
import Home from '../Home/Home'
import ELibrary from '../Elibrary/Elibraray';
import { StyleSheet, Text, View, ScrollView, Image, Dimensions,SafeAreaView } from 'react-native';
import About from '../About/About';
import Rules from '../Rules/Rules';
import React,{Component} from 'react';

export default createMaterialTopTabNavigator({
    Home: {
        screen: Home, 
        showLabel: false,
        navigationOptions: {
            header: {
                visible: false
                },
        tabBarLabel:"Home",
        tabBarIcon: ({ tintColor }) => <Icon name={"md-home"} size={20} color={tintColor} />
        }
    },  
    
    ELibrary: {
        screen: ELibrary,
        navigationOptions: {
            tabBarLabel:"E-Library",
            tabBarIcon: ({ tintColor }) => <Icon name={"md-book"} size={20} color={tintColor} />
        }
    },
    Rules: {
      screen: Rules,
      navigationOptions: {
        tabBarLabel:"Rules",
        tabBarIcon: ({ tintColor }) => <Ionicons name="md-information-circle" size={20} color={tintColor} />
        }
    },
    About: {
      screen: About,
      navigationOptions: {
        tabBarLabel:"About",
        tabBarIcon: ({ tintColor }) => <Ionicons name="md-checkmark-circle" size={20} color={tintColor} />
        }
    }
  },{
    tabBarPosition: 'top',
    lazy:true,
    tabBarOptions:{          
        showLabel: false,
        upperCaseLabel:false,
        scrollEnabled:false,
        activeTintColor: 'rgba(66, 103, 178, 0.95)',
        inactiveTintColor: '#000',
        showIcon: true,
        indicatorStyle:{
            backgroundColor: '#fff',
        }
        , style: {
           backgroundColor:'#fff',
          }

    }
  });

  