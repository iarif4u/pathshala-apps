
import React, { Component } from 'react';
import { StyleSheet, AsyncStorage, Text, View, Image, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import { Actions } from 'react-native-router-flux';
const ACCESS_TOKEN = 'accessToken';
const ACCESS_TOKEN_TYPE = 'accessTokenType';
const ACCESS_TOKEN_EXPIRES = 'accessTokenExpires';
const IS_LOGIN = 'is_login';

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email_txt: '',
      password_txt: '',
      login_error: '',
    }

  }
  async set_login_data(token, token_type, token_expire) {
    try {
      await AsyncStorage.setItem(ACCESS_TOKEN, token);
      await AsyncStorage.setItem(IS_LOGIN, 'YES');
      await AsyncStorage.setItem(ACCESS_TOKEN_TYPE, token_type);
      await AsyncStorage.setItem(ACCESS_TOKEN_EXPIRES, '' + token_expire, function () {
        Actions.loginSuccess()
      });
      //Actions.app();
    } catch (error) {
      console.log(error);
    }
  }


  async user_login(self) {
    var user_email = self.state.email_txt;
    var user_password = self.state.password_txt;
    if (this.ValidateData(user_email, user_password)) {
      this.setState({ login_error: "Email or password is invalid" });
    } else {
      try {
        const rawResponse = await fetch('http://192.168.0.108/pathshala/public/oauth/token', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            grant_type: 'password',
            client_id: 2,
            client_secret: 'VCuQxsMegnhMEidsdVpy5GVEzdGXJZzMXmZkkF2z',
            username: user_email,
            password: user_password

          })
        });
        const content = await rawResponse.json();
        console.log(rawResponse);
        if ('access_token' in content && rawResponse.status == 200 && rawResponse.ok == true) {
          this.setState({
            login_error: '',
          });
          this.set_login_data(content.access_token, content.token_type, content.expires_in)

        } else {
          this.setState({ login_error: content.message });
        }

      } catch (exceptionError) {
        this.setState({ login_error: exceptionError.login_error })
      }
    }
  }

  ValidateData(mail, password) {
    if (mail.length < 3 || password.length < 6) {
      return true;
    } else {
      return false;
    }

  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView style={Styles.container} behavior="padding" enabled>
        <Image source={require('../../assets/alorpathshala.png')} />
        <Text style={Styles.error_msg}>
          {this.state.login_error}
        </Text>
        <TextInput
          placeholder='Enter your email address'
          returnKeyLabel="Next"
          autoCapitalize='none'
          autoCorrect={false}
          keyboardType='email-address'
          onChangeText={(text) => this.setState({ email_txt: text })}
          onSubmitEditing={() => { this.passwordInput.focus() }}
          style={Styles.inputField} />
        <TextInput
          ref={(input) => { this.passwordInput = input }}
          secureTextEntry
          placeholder='Enter your password'
          returnKeyLabel="Login"
          onChangeText={(text) => this.setState({ password_txt: text })}
          style={Styles.inputField} />
        <TouchableOpacity onPress={() => this.user_login(this)} style={Styles.loginButton}>
          <Text style={Styles.loginTxt}>Login</Text>
        </TouchableOpacity>
        <View style={Styles.bottomTxt}>
          <Text style={Styles.forgetPassTxt}>
            Forget Password?
          </Text>
          <Text onPress={() => navigate('Registration', { name: 'Jane' })} style={Styles.createAccTxt}>
            Create Account
          </Text>
        </View>
      </KeyboardAvoidingView>

    );
  }
}
const width = '90%';
const height = '5%';
const Styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  inputField: {
    paddingHorizontal: 10,
    width,
    height: 40,
    backgroundColor: '#e7e7e7',
    marginBottom: 10,
    opacity: 0.95

  },
  bottomTxt: {
    width,
  },

  loginButton: {
    width,
    backgroundColor: '#00adef',
    paddingVertical: 10,
  },
  loginTxt: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: '700',
  }
  , forgetPassTxt: {
    alignContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
  },
  createAccTxt: {
    marginTop: -18,
    alignContent: 'flex-end',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
  },
  error_msg: {
    color: 'red',
    fontSize: 18,
    height: 40
  }
});