import React, { Component } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import { Header, Left } from 'native-base';

class Search extends Component {
  SearchBooks(text) {
    this.setState({
      search: text
    })
    console.log(text);
  }
  constructor(props) {
    super(props);

  }
  back() {
    Actions.app()
  }
  render() {
    return (

      <Header style={{ backgroundColor: 'rgb(66, 103, 178)' }}>
        <Left>
          <Icon style={{ paddingLeft: 5 }} onPress={() => this.back()} name={"ios-arrow-round-back"} size={30} color={"#fff"} />
        </Left>
        <View style={styles.searchSection}>
          <Icon style={styles.searchIcon} name="ios-search" size={20} color="#000" />
          <TextInput
            autoFocus={true}
            onChangeText={(text) => this.SearchBooks(text)}
            style={styles.input}
            placeholder="Search books"
          />
        </View>
      </Header>
    )
  }
}
export default Search;


const styles = {
  searchSection: {
    marginLeft: 15,
    flex: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(66, 103, 178, 0.95)',
    borderBottomColor: '#e7e7e7',
    borderBottomWidth: 0.5,
    marginBottom: 10,
    paddingTop: 10,
  },
  searchIcon: {
    paddingRight: 10,
    color: '#fff',
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 5,
    paddingRight: 10,
    paddingLeft: 0,
    backgroundColor: 'rgb(66, 103, 178)',
    color: '#fff',
    fontSize: 19,
  },

}