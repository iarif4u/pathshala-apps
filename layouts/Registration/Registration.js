
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
export default class Registration extends Component {
  constructor(props) {
    super(props);
  }
  render() {

    const { navigate } = this.props.navigation;

    return (
      <KeyboardAvoidingView style={Styles.container} behavior="padding" enabled>
        <Image source={require('../../assets/alorpathshala.png')} />

        <TextInput
          placeholder='Enter your Name'
          autoCapitalize='none'
          autoCorrect={false}
          onSubmitEditing={() => { this.phoneInput.focus() }}
          style={Styles.inputField} />

        <TextInput
          placeholder='Enter your phone number'
          keyboardType='decimal-pad'
          ref={(input) => { this.phoneInput = input }}
          onSubmitEditing={() => { this.emailInput.focus() }}
          style={Styles.inputField} />

        <TextInput
          placeholder='Enter your email address'
          returnKeyLabel="Next"
          autoCapitalize='none'
          autoCorrect={false}
          keyboardType='email-address'
          ref={(input) => { this.emailInput = input }}
          onSubmitEditing={() => { this.passwordInput.focus() }}
          style={Styles.inputField} />

        <TextInput
          ref={(input) => { this.passwordInput = input }}
          secureTextEntry
          onSubmitEditing={() => { this.confirmPasswordInput.focus() }}
          placeholder='Enter your password'
          returnKeyLabel="Login"
          style={Styles.inputField} />

        <TextInput
          ref={(input) => { this.confirmPasswordInput = input }}
          onSubmitEditing={() => { this.addressInput.focus() }}
          secureTextEntry
          placeholder='Confirm your password'
          returnKeyLabel="Login"
          style={Styles.inputField} />

        <TextInput
          placeholder='Enter your address'
          multiline={true}
          autoCapitalize='none'
          ref={(input) => { this.addressInput = input }}
          autoCorrect={false}
          style={Styles.inputField} />

        <TouchableOpacity style={Styles.loginButton}>
          <Text style={Styles.loginTxt}>Registration</Text>
        </TouchableOpacity>
        <Text onPress={() => navigate('Login', { name: 'Jane' })} style={Styles.loginBtnTxt}>
          Already have an accout? Login
          </Text>
      </KeyboardAvoidingView>

    );
  }
}

const width = '90%';
const height = '5%';
const Styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  inputField: {
    width,
    height: 40,
    paddingHorizontal: 10,
    backgroundColor: '#e7e7e7',
    marginBottom: 10,
    opacity: 0.95
  },
  loginButton: {
    backgroundColor: '#00adef',
    paddingVertical: 10,
    width,
  },
  loginTxt: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: '700',
  },
  loginBtnTxt: {
    padding: 10,
  }
});