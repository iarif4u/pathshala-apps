
import React,{Component} from 'react';
import {Actions} from 'react-native-router-flux';
import { AsyncStorage} from 'react-native';
import Loading from './../Loading';
const ACCESS_TOKEN = 'accessToken';
const ACCESS_TOKEN_TYPE = 'accessTokenType';
const ACCESS_TOKEN_EXPIRES = 'accessTokenExpires';
const IS_LOGIN = 'is_login';

export default class Logout extends Component {
    constructor(props){      
        super(props);
        this.clear_login_data()
        Actions.rootScene();
    }
    async clear_login_data(){
        try{
            await AsyncStorage.removeItem(ACCESS_TOKEN);
            await AsyncStorage.removeItem(IS_LOGIN);
            await AsyncStorage.removeItem(ACCESS_TOKEN_TYPE);
            await AsyncStorage.removeItem(ACCESS_TOKEN_EXPIRES);
        }catch(error){
          console.log(error);
        }
      }

    render() {
      return (
        <Loading/>
      );
    }
  }