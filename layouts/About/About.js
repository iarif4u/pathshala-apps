
import React,{Component} from 'react';
import { StyleSheet, Text, View, SafeAreaView,WebView } from 'react-native';
import Loading from './../Loading'
export default class About extends Component {
  constructor(props){
    super(props);
    this.state = {
      loaded : false,
      about_title : '',
      about_text: '',
      isLoading: true
    }
  }

  componentDidUpdate(){
    if(this.state.isLoading){
      setTimeout(()=>{
        this.setState({
          isLoading: false,
        });
      },1000);
    }
  }

  componentWillMount(){
    fetch('http://alorpathshala.org/api/about')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          about_title: responseJson.title,
          about_text:responseJson.content
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

    render() {
      if(!this.state.isLoading){
      return (
        <WebView
          style={{margin:15,paddingVertical:15}}
          originWhitelist={['*']}
          source={{ html: this.state.about_text }}
        />
      );
    }else{
     return <Loading/>
    }
    }
  }