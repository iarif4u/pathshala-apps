
import React,{Component} from 'react';
import {  createAppContainer,createDrawerNavigator,DrawerItems,DrawerActions } from 'react-navigation';
import AppNavigation from './layouts/navigation/AppNavigation';
import {StatusBar, Text, View, TouchableOpacity, SafeAreaView, ScrollView,AsyncStorage,Image } from 'react-native';
import Search from './layouts/Search/Search';
import Icon from 'react-native-vector-icons/Ionicons';
import Login from './layouts/Login/Login';
import Logout from './layouts/Logout/Logout';
import Registration from './layouts/Registration/Registration';
import { Router, Scene,Actions } from 'react-native-router-flux';
import {Header,Left,Right, Input} from 'native-base';
import { TextInput } from 'react-native-gesture-handler';
import Book from './layouts/Book/Book';
const TopNavigator = createAppContainer(AppNavigation);

const customDrawerComponent = (props) =>
  
  (
  <SafeAreaView style={{flex:1}}>
    <View style={{alignContent:'center',alignItems:'center',margin:10}}>
      <Image style={{height:130,width:130,padding:50}} source={require('./assets/alorpathshala.png')} />
    </View>
    <ScrollView>
      <DrawerItems {...props}/>      
    </ScrollView>
  </SafeAreaView>
  )
const styles ={
  searchSection: {
    marginLeft: 15,
    flex:4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(66, 103, 178, 0.95)',
    borderBottomColor: '#e7e7e7',
    borderBottomWidth: 0.5,
    marginBottom: 10,
    paddingTop:  10,
},
searchIcon: {
  paddingRight: 10,
    color:'#fff',
},
input: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 5,
    paddingRight: 10,
    paddingLeft: 0,
    backgroundColor: 'rgb(66, 103, 178)',
    color: '#fff',
    fontSize:19,
},

};
class TopNaviation extends Component {
  
  constructor(props){
    super(props);

  }
  static navigationOptions = {
    title: 'Home',
  };
    render() {    
      return (   
        <View style={{flex:1,flexDirection:'column'}}>
        <Header style={{backgroundColor:'rgb(66, 103, 178)'}}>    
        <Left>
        <Icon onPress={()=>this.props.navigation.dispatch(DrawerActions.openDrawer())} name={"ios-menu"} size={30} color={"#fff"}  />
        </Left>    
       
        <View style={styles.searchSection}>
            <Icon style={styles.searchIcon} name="ios-search" size={20} color="#000"/>
            <TextInput
            onFocus={ () => Actions.search()} 
                style={styles.input}
                placeholder="Search books"
            />
        </View>
        </Header>  
        <TopNavigator/>    
         </View>   
                  
      );
    }
}

const Routes = () => (
  <Router navigationBarStyle={{ backgroundColor: 'rgba(66, 103, 178, 0.95)',height:20, }}>
     <Scene key = "root">
        <Scene hideNavBar={true} key = "app" component = {App} title = "App" initial = {true} />
        <Scene hideNavBar={true} key="rootScene" component = {RootScene} title = "RootScene" />
        <Scene hideNavBar={true} key="search" component = {Search} title = "Search" />
        <Scene hideNavBar={true} key="loginSuccess" component = {LoginSuccess} title = "loginSuccess" />
        <Scene hideNavBar={true} key="book" component = {Book} title = "book" />
     </Scene>
  </Router>
)

export default Routes;
class RootScene extends Component {   
  constructor(props){
    super(props);
  }
    render() {   
        return (                
          <PrimaryNavigation/>            
        );      
    }
}
class LoginSuccess extends Component {   
  constructor(props){
    super(props);
  }
    render() {   
        return (                
          <SecondaryNavigation/>            
        );      
    }
}
class App extends Component {   
  constructor(props){
    super(props);
    this.state = {
      is_login: false,
      access_token : ''
    }
  }
    async componentDidMount(){
      let storages_key = await AsyncStorage.getItem('is_login');      
      if (storages_key === 'YES') {
        console.log("Login Success...");
        Actions.loginSuccess();
      }
    }

    render() {   
      if(this.state.is_login==false){
        return (         
          <PrimaryNavigation/>   
        );
      }else{
        return (                
          <SecondaryNavigation/>            
        );
      }
    }
}
const naviationConfig = {
  contentComponent:customDrawerComponent,
  initialRouteName: 'Home',
  drawerOpenRoute: "DrawerOpen",
  drawerCloseRoute: "DrawerClose",
  drawerToggleRoute: "DrawerToggle",
  drawerBackgroundColor: "rgba(66, 103, 178, 0.95)",
  contentOptions: {
    activeTintColor :'#ffffff',
     inactiveTintColor :'#ffffff',
    activeBackgroundColor :'rgb(59, 93, 160)',
  }
};

const SecondaryNavigation =  createAppContainer(createDrawerNavigator({
  Home: {
      screen: TopNaviation, 
      navigationOptions: {
      tabBarLabel:"Home",
      drawerIcon: ({ tintColor }) => <Icon name={"ios-home"} size={20} color={tintColor} />
      }
  },  
  Logout: {
    screen: Logout, 
    tabBarLabel:"Logout",
    navigationOptions:{
      drawerIcon: ({tintColor})=> <Icon name={"md-log-out"} size={20} color={'#fff'} />
    }
  }
},naviationConfig));

const PrimaryNavigation =  createAppContainer(createDrawerNavigator({
  
  Home: {
      screen: TopNaviation, 
      navigationOptions: {
      tabBarLabel:"Home",
      drawerIcon: ({ tintColor }) => <Icon name={"ios-home"} size={20} color={tintColor} />
      }
  },
  Login:{
    screen: Login,      
    navigationOptions: {
      tabBarLabel:"Login",
      drawerIcon: ({ tintColor }) => <Icon name={"ios-log-in"} size={20} color={tintColor} />
      }
  },
  Registration:{
    screen: Registration,      
    navigationOptions: {
      tabBarLabel:"Registration",
      drawerIcon: ({ tintColor }) => <Icon name={"md-person-add"} size={20} color={tintColor} />
      }
  },
  
},naviationConfig));
